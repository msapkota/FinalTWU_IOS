//
//  TWUBucketlist+CoreDataProperties.swift
//  FinalTWU
//
//  Created by Mahesh Sapkota, Sarad Poudel, Kritartha Kafle on 3/6/21.
//
//

import Foundation
import CoreData


extension TWUBucketlist {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TWUBucketlist> {
        return NSFetchRequest<TWUBucketlist>(entityName: "TWUBucketlist")
    }

    @NSManaged public var name: String?
    @NSManaged public var createdon: Date?

}

extension TWUBucketlist : Identifiable {

}
