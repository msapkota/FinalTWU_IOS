//
//  ViewController.swift
//  FinalTWU
//
//  Created by Mahesh Sapkota, Sarad Poudel, Kritartha Kafle on 3/6/21.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /*let date = Date()
        let formatter = DateFormatter()
        formatter.timeZone = .current
        formatter.locale = .current
        formatter.dateFormat = "MM/dd/yyyy"*/
        
        let model = models[indexPath.row]
        let cell = tabelView.dequeueReusableCell(withIdentifier: "cell", for: indexPath )
        cell.textLabel?.text = model.name
        //cell.accessoryType = model.Completed ? .checkmark : .none
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = models[indexPath.row]
        
        
    let sheet = UIAlertController(title: "Edit", message: nil, preferredStyle: .actionSheet)
        
        sheet.addAction(UIAlertAction(title: "Cancel ", style: .cancel, handler: nil))
        
        sheet.addAction(UIAlertAction(title: "Edit", style: .default, handler: { _ in
            let alert = UIAlertController(title: "Edit Items", message: "edit your Item", preferredStyle: .alert)
            alert.addTextField(configurationHandler: nil)
            alert.textFields?.first?.text = item.name
            alert.addAction(UIAlertAction(title: "Done", style: .cancel, handler: { [weak self] _ in
                guard let field = alert.textFields?.first, let newName = field.text, !newName.isEmpty
                else{
                    return
                }
                self?.updateItem(item: item, newName: newName)
            }))
            self.present(alert, animated: true)
            
        }))
        sheet.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { [weak self] _ in
            self?.deleteItem(item: item)
        }))
       
    present(sheet, animated: true)
    }

    
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    let tabelView: UITableView = {
        let tabel = UITableView()
        tabel.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        return tabel
    }()
    private var models = [TWUBucketlist]()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "TWUBucketlist"
        view.addSubview(tabelView)
        getALLItems()
        tabelView.delegate = self
        tabelView.dataSource = self
        tabelView.frame = view.bounds
        
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(AddIsPressed))
    }
    @objc private func AddIsPressed(){
        let alert = UIAlertController(title: "Add New Items", message: "Please enter New Item", preferredStyle: .alert)
        alert.addTextField(configurationHandler: nil)
        alert.addAction(UIAlertAction(title: "Add", style: .cancel, handler: { [weak self] _ in
            guard let field = alert.textFields?.first, let text = field.text, !text.isEmpty
            else{
                return
            }
            self?.createItem(name: text)
        }))
        present(alert, animated: true)
    }
    func getALLItems() {
        
        do
        {
            models = try context.fetch(TWUBucketlist.fetchRequest())
            
            DispatchQueue.main.async {
                self.tabelView.reloadData()
            }
            
        }
        catch {
            //error
        }
        
    }
    
    func createItem(name: String) {
        let newItem = TWUBucketlist(context: context)
        newItem.name = name
        newItem.createdon = Date()
        
        do{
            try context.save()
            getALLItems()
        }
        catch{
            
        }
    }
    
    func deleteItem(item: TWUBucketlist) {
        context.delete(item)
        do{
            try context.save()
            getALLItems()
        }
        catch{
            
        }
        
    }
    
    func updateItem(item: TWUBucketlist, newName: String) {
        item.name = newName
        do{
            try context.save()
            getALLItems()
        }
        catch{
            
        }
    }
}

